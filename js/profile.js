function loadProfile() {
	loadProfileInfo();
}

function loadProfileInfo() {
	$.ajax({
		type: "GET",
		url: 'http://sumjup-1384.appspot.com/api/user/get_info',
		data: {
			user_id: CHOOSEN_PROFILE_USER_ID
		},
		success: function(data) {
			if (data['status'] == 'failure') {
				alert(data['error_str']);
			} else {
				
				var nameHeader = document.getElementById("nameHeader");
				var userBirthdayField = document.getElementById("userInfoBirthDay");
				var userPhoneField = document.getElementById("userInfoPhone");
				
				CHOSEN_PROFILE_INFO['name'] = data['user_info']['name'];
				CHOSEN_PROFILE_INFO['surname'] = data['user_info']['surname'];
				CHOSEN_PROFILE_INFO['birthDate'] = data['user_info']['birth_date'];
				
				
				
				nameHeader.innerHTML = data['user_info']['name'] + ' ' + data['user_info']['surname'];
				userBirthdayField.innerHTML = data['user_info']["birth_date"]["day"] + "-" + data['user_info']["birth_date"]["month"] + "-" + data['user_info']["birth_date"]["year"];
				userPhoneField.innerHTML = data['user_info']["phone"];
			}
		}
	})
}

function startChat() {
	$.ajax({
		type: "POST",
		url: "http://sumjup-1384.appspot.com/api/dialog/start",
		data: {
			token: CURRENT_USER_TOKEN,
			recipient_id: CHOOSEN_PROFILE_USER_ID
		},
		success: function(data) {
			if (data['status'] == 'failure') {
				alert(data['error_str'])
			} else {
				CHOOSEN_CHAT_ID = data['dialog_info']['id'];
				tau.changePage("chat.html");
			}
		}
	});
}