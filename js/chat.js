function loadChat() {
	getDialog();
}


function getDialog() {
	
	var content = document.getElementById('chatListView');
	content.innerHTML = "";
	
	$.ajax({
		type: "POST",
		url: "http://sumjup-1384.appspot.com/api/dialog/info",
		data: {
			token: CURRENT_USER_TOKEN,
			dialog_id: CHOOSEN_CHAT_ID
		},
		success: function(data) {
			
			if (data['status'] == 'failure') {
				alert(data['error_str']);
			} else {
				var messages = data['dialog_info']['messages']
				
				for (var i = 0; i < messages.length; ++i) {
					var message = messages[i];
					
					if (message['sender_id'] == CURRENT_USER_ID) {
						content.innerHTML += '<div class="message-container">';
						content.innerHTML += '<p id="chat-user-name">' + CURRENT_USER_INFO['name']  + '</p>';
						content.innerHTML += '<p class="message">' + message['text'] + '</p>';
						content.innerHTML += '</div><br>';
					} else {
						content.innerHTML += '<div class="message-container">';
						content.innerHTML += '<p id="chat-user-name">' + CHOSEN_PROFILE_INFO['name'] + '</p>';
						content.innerHTML += '<p class="message">' + message['text'] + '</p>';
						content.innerHTML += '</div><br>';
					}
					
				}
			}
		}
	})
}

function sendMessage() {
	var messageTextContainer = document.getElementById("message-text-container");
	var messageText = messageTextContainer.value;
//	
	var content = document.getElementById('chatListView');
	
	$.ajax({
		type: "POST",
		url: "http://sumjup-1384.appspot.com/api/dialog/send",
		data: {
			token: CURRENT_USER_TOKEN,
			dialog_id: CHOOSEN_CHAT_ID,
			text: messageText
		},
		success: function(data) {
			if (data['status'] == 'failure') {
				alert(data['error_str']);
			} else {
				var message = data['message_info'];

				content.innerHTML += '<div class="message-container">';
				content.innerHTML += '<p id="chat-user-name">' + CURRENT_USER_INFO['name'] + '</p>';
				content.innerHTML += '<p class="message">' + message['text'] + '</p>';
				content.innerHTML += '</div><br>';
				
			}
			messageTextContainer.value = '';
		}
	})
}