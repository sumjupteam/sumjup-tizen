window.onload = function() {
    document.addEventListener('tizenhwkey', function(e) {
        if (e.keyName === "back") {
        	var page = document.getElementsByClassName('ui-page-active')[0];
        	var pageid = page ? page.id : "";
        	if (pageid === "startPage"){
        		try{
        			tizen.application.getCurrentApplication().exit();
        		}catch (ignore){
        		}
        	}else{
        		if (pageid !== "basicPage"){
        			window.history.back();
        		}
        	}
        }
    });
    
    document.addEventListener('pagechange', function(e){
    	var pageId = tau.activePage.getAttribute('id');
    	
    	if (pageId == "signInPage") {
    		loadSignIn();
    	} else if (pageId == "basicPage") {
    		loadBasic();
    	} else if (pageId == "profilePage") {
    		loadProfile();
    	} else if (pageId == "chatPage") {
    		loadChat();
    	}

    });
};