function loadBasic() {
	getUsers();
}



function getUsers() {
	var content = document.getElementById('nearbyPeopleListView');
	
	content.innerHTML = "";
	
	$.ajax({
		type: "POST",
		url: 'http://sumjup-1384.appspot.com/api/user/get_all',
		data: {
			token: CURRENT_USER_TOKEN
		},
		success: function(data) {
			if (data['status'] == 'failure') {
				alert(data['error_str']);
			} else {
				var users = data['users'];
		    	for (var i = 0; i < users.length; i++){
		    		content.innerHTML+= '<li><a class="ui-btn" onclick="selectProfile('+users[i]['id'] + ')"'+ '>' + users[i]["name"] + '</a></li>'; 
		    	}
			}
		}
	})
}

function selectProfile(id) {
	CHOOSEN_PROFILE_USER_ID = id;

	tau.changePage("profile.html");
}
