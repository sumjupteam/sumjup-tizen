var verCode = "";
function getCode(){
	if (checkPhoneNumber() === true){
		sendSMS();
		
	}
	else{
		return;
	}
	$("#writePhoneNumberForm").hide();
	$("#codeButtonSignUp").attr("onclick", "sendCode();");
	$("#codeButtonSignUp").attr('value', 'Send code');
	$("#writeCodeForm").show();
}
function sendCode(){
	if (document.getElementById("InputCodeSignUp").value === verCode){
		$("#userInfoForm").show();
		$("#writeCodeForm").hide();
		$("#codeButtonSignUp").hide();
		$("writePhoneNumberForm").hide();
	}else{
		alert("Wrong code")
	}
	
}

function sendSMS(){
	verCode = generateCode();
	var req = new XMLHttpRequest();
	var phone = document.getElementById("InputPhoneNumberSignUp").value;
	
	req.open("GET", "http://sms.ru/sms/send?api_id=BDFCCE5E-FCD2-AE24-3991-B02C508215CE&to=" + phone + "&text=" + verCode, false);
	req.send(null);
}

function generateCode(){
	var text = "";
	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	for( var i=0; i < 7; i++ )
	text += possible.charAt(Math.floor(Math.random() * possible.length));

	return text;
}



function checkNameSignUp(){

	var name = document.getElementById("InputNameSignUp");
	if (name.value === ""){
		document.getElementById("LabelSignUpName").style.color = "red";
		alert("Write your name");
		return false;
		
	}else{
		document.getElementById("LabelSignUpName").style.color = "black";
		return true;
	}
}

function checkDateSignUp(){
	var date = document.getElementById("InputDateSignUp");
	if (date.value === ""){
		document.getElementById("LabelSignUpAge").style.color = "red";
		alert("Write your birthday");
		return false;
	}else{
		document.getElementById("LabelSignUpAge").style.color = "black";
		return true;
	}
}


function checkPasswordSignUp(){

	var pass = document.getElementById("InputPasswordSignUp");
	if (pass.value === ""){
		document.getElementById("LabelSignUpPassword").style.color = "red";
		alert("Write your password");
		return false;
		
	}else{
		document.getElementById("LabelSignUpPassword").style.color = "black";
		return true;
	}
}
function checkFields(){
	
	if (checkNameSignUp() === false){
		return false;
	}
	
	if (checkDateSignUp() === false){
		return false;
	}
	
	if (checkPasswordSignUp() === false){
		return false;
	}
		
	return true;
}


function checkUserSignUpInformation(){
	if (checkFields() === false){
		return;
	}
	var s = "";
	if (document.getElementById("selectSexSignUp").value === "Female"){
		s = "0";
	}else{
		s = "1";
	}

	$.ajax({
		  type: "POST",
		  url: 'http://sumjup-1384.appspot.com/api/auth/sign_up',
		  data: {
			  phone: document.getElementById("InputPhoneNumberSignUp").value,
			  password: document.getElementById("InputPasswordSignUp").value, 
			  name: document.getElementById("InputNameSignUp").value, 
			  sex: s, 
			  birth_date: document.getElementById("InputDateSignUp").value
		  },
		  success: function(msg){
		    if (msg["status"] === "failure"){
		    	alert(msg["error_str"]);
		    	tau.changePage("index.html");
		    }else{
		    	tau.changePage("signIn.html");
		    }
		  },
		  error: function(data){
			  alert("error");
			  alert(data);
		  }
		});	
}

function checkPhoneNumber(){
	var phone = document.getElementById("InputPhoneNumberSignUp");
	if (phone.value.length !== 11){
		alert("Wrong phone number");
		return false;
	}
	if (phone.value[0] !== "7"){
		alert("First number should be 7");
		return false;
	}
	return true;
}